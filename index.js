const express = require('express')
const mongoose = require('mongoose')
const app = express()
const port = 3002

mongoose.connect(`mongodb+srv://admin123:admin123@zuitt-bootcamp.cnhvgta.mongodb.net/S35-Activity?retryWrites=true&w=majority`, 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	}
)

let db = mongoose.connection

db.on('error', console.log.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))

// #1 CREATE A SCHEMA
const registerSchema = new mongoose.Schema({
	username: String, 
	password: String
})

// #2 CREATE A USER MODEL
const Register = mongoose.model('Register', registerSchema)

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// #3 CREATE A POST ROUTE TO ACCESS /signup TO CREATE USER
app.post('/signup', (request, response) => {
	let newRegister = new Register ({
		username : request.body.username,
		password: request.body.password
	})

	newRegister.save((error, savedRegister) => {
		return response.status(201).send('You have successfully registered!')
	})
})

app.get('/signup', (request, response) => {
	return Register.find({}, (error, result) => {
		if(error){
			response.send(error)
		}
		response.send(result)
	})
})

app.listen(port, () => console.log(`Server is running at port ${port}`))